# Pixelfont Gallery

Renders pixelfonts in micropython. Why do this? Pixelfonts are often more legible at small sizes than vector fonts. Also, you can add new fonts without modifying the firmware.

## How to use in your own app

Copy writer_card10.py and any fonts to your app folder and import them. Fonts take a lot of RAM, so it's best not to use too many. Importing all the fonts in the demo at once will crash your app!

Use the TextBuffer and MultilineTextBuffer classes to render your text. Both of these internally handle FrameBuffers, so it's best to not make too many of these objects often.

TextBuffer displays a single line of text. It's perfect for sensor readouts and similar displays. It only rerenders the text if it changes, so putting it on the display is very fast otherwise.

MultilineTextBuffer is for displaying bigger amounts of text, like instructions or notifications. You can construct writers with different fonts and mix them, for creating bold text and similar effects.

If you want to encode your own fonts, take a look at the [micropython-font-to-py](https://github.com/peterhinch/micropython-font-to-py) repository. Be aware that you have to use the -x flag when encoding!

## Attribution/Open Source Licenses

This project includes work done by other people, and released under permissive licenses. Specifically, these lovely people:

- [Peter Hinch](https://github.com/peterhinch)
  
  - [micropython-font-to-py: A Python 3 utility to convert fonts to Python source capable of being frozen as bytecode](https://github.com/peterhinch/micropython-font-to-py), (released under *MIT License*)

- [Slavfox](https://github.com/slavfox)
  
  - [Cozette: A bitmap programming font optimized for coziness 💜](https://github.com/slavfox/Cozette) (released under *MIT License*)

- [Rob Mocci](https://www.patreon.com/rmocci)
  
  - [Dogica](https://rmocci.itch.io/dogica) (released under *SIL Open Font License*)
  
  - [Dogica bold](https://rmocci.itch.io/dogica-bold) (released under *SIL Open Font License*)

- [Hugo Chargois](https://github.com/hchargois)
  
  - [GitHub - hchargois/gohufont: A monospace bitmap font.](https://github.com/hchargois/gohufont) (released under *Do What The Fuck You Want To Public License*)

- [Emily Huo](https://emhuo.itch.io/)
  
  - [Peaberry Pixel Font](https://emhuo.itch.io/peaberry-pixel-font) (released under *SIL Open Font License*)
  
  - [RaccoonSerif Pixel Font](https://emhuo.itch.io/raccoonserif-pixel-font) (released under *SIL Open Font License*)
