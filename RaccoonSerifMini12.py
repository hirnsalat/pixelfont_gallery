# Code generated by font_to_py.py.
# Font: RaccoonSerifMini12.ttf
# Cmd: ../font_to_py.py -x in/RaccoonSerifMini12.ttf 12 out/RaccoonSerifMini12.py
version = '0.33'

def height():
    return 12

def baseline():
    return 8

def max_width():
    return 10

def hmap():
    return True

def reverse():
    return False

def monospaced():
    return False

def min_ch():
    return 32

def max_ch():
    return 126

_font =\
b'\x06\x00\x00\x30\x48\x08\x10\x20\x00\x20\x00\x00\x00\x00\x05\x00'\
b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x03\x00\x00\x40'\
b'\x40\x40\x40\x40\x00\x40\x00\x00\x00\x00\x05\x00\x00\x50\x50\x50'\
b'\x00\x00\x00\x00\x00\x00\x00\x00\x08\x00\x00\x00\x14\x7e\x28\x28'\
b'\xfc\x50\x00\x00\x00\x00\x06\x00\x20\x70\xa8\xa0\x70\x28\xa8\x70'\
b'\x20\x00\x00\x00\x0a\x00\x00\x00\x00\x00\x31\x00\x4a\x00\x34\x00'\
b'\x0b\x00\x14\x80\x23\x00\x00\x00\x00\x00\x00\x00\x00\x00\x08\x00'\
b'\x00\x30\x48\x48\x30\x48\x44\x3a\x00\x00\x00\x00\x04\x00\x00\x20'\
b'\x20\x20\x00\x00\x00\x00\x00\x00\x00\x00\x04\x00\x00\x20\x40\x80'\
b'\x80\x80\x80\x40\x20\x00\x00\x00\x04\x00\x00\x80\x40\x20\x20\x20'\
b'\x20\x40\x80\x00\x00\x00\x05\x00\x00\x90\x60\xf0\x60\x90\x00\x00'\
b'\x00\x00\x00\x00\x06\x00\x00\x00\x20\x20\xf8\x20\x20\x00\x00\x00'\
b'\x00\x00\x04\x00\x00\x00\x00\x00\x00\x00\x00\x60\x20\x40\x00\x00'\
b'\x06\x00\x00\x00\x00\x00\xf8\x00\x00\x00\x00\x00\x00\x00\x03\x00'\
b'\x00\x00\x00\x00\x00\x00\x00\x40\x00\x00\x00\x00\x05\x00\x00\x00'\
b'\x10\x20\x20\x40\x40\x80\x00\x00\x00\x00\x06\x00\x00\x70\x88\x98'\
b'\xa8\xc8\x88\x70\x00\x00\x00\x00\x07\x00\x00\x10\x30\x50\x10\x10'\
b'\x10\x7c\x00\x00\x00\x00\x06\x00\x00\x70\x88\x08\x30\x40\x88\xf8'\
b'\x00\x00\x00\x00\x06\x00\x00\x70\x88\x08\x30\x08\x88\x70\x00\x00'\
b'\x00\x00\x07\x00\x00\x18\x28\x48\x88\xfc\x08\x1c\x00\x00\x00\x00'\
b'\x06\x00\x00\xf8\x80\x80\xf0\x08\x88\x70\x00\x00\x00\x00\x06\x00'\
b'\x00\x70\x88\x80\xf0\x88\x88\x70\x00\x00\x00\x00\x06\x00\x00\xf8'\
b'\x88\x10\x20\x40\x40\x40\x00\x00\x00\x00\x06\x00\x00\x70\x88\x88'\
b'\x70\x88\x88\x70\x00\x00\x00\x00\x06\x00\x00\x70\x88\x88\x88\x78'\
b'\x08\x70\x00\x00\x00\x00\x03\x00\x00\x00\x00\x40\x00\x00\x00\x40'\
b'\x00\x00\x00\x00\x04\x00\x00\x00\x00\x60\x60\x00\x00\x60\x20\x40'\
b'\x00\x00\x06\x00\x00\x00\x00\x18\x60\x80\x60\x18\x00\x00\x00\x00'\
b'\x06\x00\x00\x00\x00\xf8\x00\xf8\x00\x00\x00\x00\x00\x00\x06\x00'\
b'\x00\x00\x00\xc0\x30\x08\x30\xc0\x00\x00\x00\x00\x06\x00\x00\x30'\
b'\x48\x08\x10\x20\x00\x20\x00\x00\x00\x00\x0a\x00\x00\x00\x00\x00'\
b'\x1e\x00\x21\x00\x4c\x80\x52\x80\x52\x80\x4f\x80\x21\x00\x1e\x00'\
b'\x00\x00\x00\x00\x07\x00\x00\x10\x28\x28\x44\x7c\x44\xc6\x00\x00'\
b'\x00\x00\x07\x00\x00\xf0\x88\x88\xf8\x84\x84\xf8\x00\x00\x00\x00'\
b'\x08\x00\x00\x38\x44\x86\x80\x80\x44\x38\x00\x00\x00\x00\x08\x00'\
b'\x00\xf8\x84\x82\x82\x82\x84\xf8\x00\x00\x00\x00\x07\x00\x00\xfc'\
b'\x84\x80\xf0\x80\x84\xfc\x00\x00\x00\x00\x08\x00\x00\x7e\x42\x40'\
b'\x78\x40\x40\xe0\x00\x00\x00\x00\x08\x00\x00\x38\x44\x80\x9e\x84'\
b'\x44\x38\x00\x00\x00\x00\x09\x00\x00\x00\xc3\x00\x42\x00\x42\x00'\
b'\x7e\x00\x42\x00\x42\x00\xc3\x00\x00\x00\x00\x00\x00\x00\x00\x00'\
b'\x06\x00\x00\xf8\x20\x20\x20\x20\x20\xf8\x00\x00\x00\x00\x07\x00'\
b'\x00\x7c\x10\x10\x10\x90\x90\x60\x00\x00\x00\x00\x08\x00\x00\xc6'\
b'\x48\x50\x60\x50\x48\xc6\x00\x00\x00\x00\x08\x00\x00\xe0\x40\x40'\
b'\x40\x40\x42\x7e\x00\x00\x00\x00\x0a\x00\x00\x00\x41\x00\x63\x00'\
b'\x55\x00\x49\x00\x41\x00\x41\x00\xe3\x80\x00\x00\x00\x00\x00\x00'\
b'\x00\x00\x0a\x00\x00\x00\x61\x80\x51\x00\x51\x00\x49\x00\x45\x00'\
b'\x45\x00\xe3\x00\x00\x00\x00\x00\x00\x00\x00\x00\x08\x00\x00\x38'\
b'\x44\x82\x82\x82\x44\x38\x00\x00\x00\x00\x08\x00\x00\xfc\x42\x42'\
b'\x7c\x40\x40\xe0\x00\x00\x00\x00\x09\x00\x00\x00\x38\x00\x44\x00'\
b'\x82\x00\x82\x00\x8a\x00\x44\x00\x3b\x00\x00\x00\x00\x00\x00\x00'\
b'\x00\x00\x09\x00\x00\x00\x7c\x00\x42\x00\x42\x00\x7c\x00\x42\x00'\
b'\x42\x00\xc3\x00\x00\x00\x00\x00\x00\x00\x00\x00\x06\x00\x00\x70'\
b'\x88\x80\x70\x08\x88\x70\x00\x00\x00\x00\x06\x00\x00\xf8\x20\x20'\
b'\x20\x20\x20\x20\x00\x00\x00\x00\x0a\x00\x00\x00\xe3\x80\x41\x00'\
b'\x41\x00\x41\x00\x41\x00\x22\x00\x1c\x00\x00\x00\x00\x00\x00\x00'\
b'\x00\x00\x08\x00\x00\xc6\x44\x44\x28\x28\x10\x10\x00\x00\x00\x00'\
b'\x0a\x00\x00\x00\xc1\x80\x41\x00\x41\x00\x2a\x00\x2a\x00\x2a\x00'\
b'\x14\x00\x00\x00\x00\x00\x00\x00\x00\x00\x08\x00\x00\xc6\x44\x28'\
b'\x10\x28\x44\xc6\x00\x00\x00\x00\x08\x00\x00\xc6\x44\x44\x38\x10'\
b'\x10\x38\x00\x00\x00\x00\x07\x00\x00\xfc\x84\x08\x10\x20\x44\xfc'\
b'\x00\x00\x00\x00\x04\x00\x00\xe0\x80\x80\x80\x80\x80\x80\xe0\x00'\
b'\x00\x00\x05\x00\x00\x00\x80\x40\x40\x20\x20\x10\x00\x00\x00\x00'\
b'\x04\x00\x00\xe0\x20\x20\x20\x20\x20\x20\xe0\x00\x00\x00\x06\x00'\
b'\x10\x28\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x06\x00\x00\x00'\
b'\x00\x00\x00\x00\x00\x00\xf8\x00\x00\x00\x04\x00\x40\x60\x20\x00'\
b'\x00\x00\x00\x00\x00\x00\x00\x00\x07\x00\x00\x00\x00\x70\x88\x78'\
b'\x88\x7c\x00\x00\x00\x00\x07\x00\x00\xe0\x40\x78\x44\x44\x44\x78'\
b'\x00\x00\x00\x00\x06\x00\x00\x00\x00\x70\x88\x80\x88\x70\x00\x00'\
b'\x00\x00\x07\x00\x00\x0c\x08\x78\x88\x88\x88\x78\x00\x00\x00\x00'\
b'\x06\x00\x00\x00\x00\x70\x88\xf8\x80\x78\x00\x00\x00\x00\x06\x00'\
b'\x00\x30\x48\x40\xf0\x40\x40\xe0\x00\x00\x00\x00\x07\x00\x00\x00'\
b'\x00\x7c\x88\x88\x88\x78\x08\x88\x88\x70\x08\x00\x00\xc0\x40\x78'\
b'\x44\x44\x44\xe6\x00\x00\x00\x00\x04\x00\x00\x40\x00\xc0\x40\x40'\
b'\x40\xe0\x00\x00\x00\x00\x05\x00\x00\x10\x00\x30\x10\x10\x10\x10'\
b'\x90\x60\x00\x00\x07\x00\x60\x40\x48\x50\x60\x50\x48\xcc\x00\x00'\
b'\x00\x00\x03\x00\x00\xc0\x40\x40\x40\x40\x40\xc0\x00\x00\x00\x00'\
b'\x0a\x00\x00\x00\x00\x00\x00\x00\xf6\x00\x49\x00\x49\x00\x49\x00'\
b'\xc9\x80\x00\x00\x00\x00\x00\x00\x00\x00\x07\x00\x00\x00\x00\xb0'\
b'\x48\x48\x48\xcc\x00\x00\x00\x00\x06\x00\x00\x00\x00\x70\x88\x88'\
b'\x88\x70\x00\x00\x00\x00\x07\x00\x00\x00\x00\xf8\x44\x44\x44\x78'\
b'\x40\xc0\x00\x00\x09\x00\x00\x00\x00\x00\x00\x00\x3e\x00\x44\x00'\
b'\x44\x00\x44\x00\x3c\x00\x04\x00\x0f\x00\x00\x00\x00\x00\x06\x00'\
b'\x00\x00\x00\xf0\x48\x48\x40\xe0\x00\x00\x00\x00\x06\x00\x00\x00'\
b'\x00\x38\x48\x30\x88\x70\x00\x00\x00\x00\x04\x00\x00\x40\x40\xe0'\
b'\x40\x40\x40\x20\x00\x00\x00\x00\x08\x00\x00\x00\x00\xc4\x44\x44'\
b'\x4c\x36\x00\x00\x00\x00\x08\x00\x00\x00\x00\xc6\x44\x44\x28\x10'\
b'\x00\x00\x00\x00\x08\x00\x00\x00\x00\xc6\x44\x54\x28\x28\x00\x00'\
b'\x00\x00\x06\x00\x00\x00\x00\xd8\x50\x20\x50\xd8\x00\x00\x00\x00'\
b'\x07\x00\x00\x00\x00\x64\x24\x28\x10\x10\x20\xc0\x00\x00\x06\x00'\
b'\x00\x00\x00\xf8\x90\x20\x48\xf8\x00\x00\x00\x00\x04\x00\x00\x20'\
b'\x40\x40\x40\x80\x40\x40\x40\x20\x00\x00\x03\x00\x00\x40\x40\x40'\
b'\x40\x40\x40\x40\x40\x00\x00\x00\x04\x00\x00\x80\x40\x40\x40\x20'\
b'\x40\x40\x40\x80\x00\x00\x07\x00\x00\x00\x00\x00\x64\x98\x00\x00'\
b'\x00\x00\x00\x00'

_index =\
b'\x00\x00\x0e\x00\x1c\x00\x2a\x00\x38\x00\x46\x00\x54\x00\x6e\x00'\
b'\x7c\x00\x8a\x00\x98\x00\xa6\x00\xb4\x00\xc2\x00\xd0\x00\xde\x00'\
b'\xec\x00\xfa\x00\x08\x01\x16\x01\x24\x01\x32\x01\x40\x01\x4e\x01'\
b'\x5c\x01\x6a\x01\x78\x01\x86\x01\x94\x01\xa2\x01\xb0\x01\xbe\x01'\
b'\xcc\x01\xda\x01\xf4\x01\x02\x02\x10\x02\x1e\x02\x2c\x02\x3a\x02'\
b'\x48\x02\x56\x02\x70\x02\x7e\x02\x8c\x02\x9a\x02\xa8\x02\xc2\x02'\
b'\xdc\x02\xea\x02\xf8\x02\x12\x03\x2c\x03\x3a\x03\x48\x03\x62\x03'\
b'\x70\x03\x8a\x03\x98\x03\xa6\x03\xb4\x03\xc2\x03\xd0\x03\xde\x03'\
b'\xec\x03\xfa\x03\x08\x04\x16\x04\x24\x04\x32\x04\x40\x04\x4e\x04'\
b'\x5c\x04\x6a\x04\x78\x04\x86\x04\x94\x04\xa2\x04\xb0\x04\xca\x04'\
b'\xd8\x04\xe6\x04\xf4\x04\x0e\x05\x1c\x05\x2a\x05\x38\x05\x46\x05'\
b'\x54\x05\x62\x05\x70\x05\x7e\x05\x8c\x05\x9a\x05\xa8\x05\xb6\x05'\
b'\xc4\x05'

_mvfont = memoryview(_font)
_mvi = memoryview(_index)
ifb = lambda l : l[0] | (l[1] << 8)

def get_ch(ch):
    oc = ord(ch)
    ioff = 2 * (oc - 32 + 1) if oc >= 32 and oc <= 126 else 0
    doff = ifb(_mvi[ioff : ])
    width = ifb(_mvfont[doff : ])

    next_offs = doff + 2 + ((width - 1)//8 + 1) * 12
    return _mvfont[doff + 2:next_offs], 12, width
 
