# Code generated by font_to_py.py.
# Font: DogicaBold8.ttf
# Cmd: ../font_to_py.py -x in/DogicaBold8.ttf 8 out/DogicaBold8.py
version = '0.33'

def height():
    return 8

def baseline():
    return 7

def max_width():
    return 10

def hmap():
    return True

def reverse():
    return False

def monospaced():
    return False

def min_ch():
    return 32

def max_ch():
    return 126

_font =\
b'\x08\x00\x3c\x66\x06\x1c\x30\x00\x18\x00\x04\x00\x00\x00\x00\x00'\
b'\x00\x00\x00\x00\x04\x00\x60\x60\x60\x60\x60\x00\x60\x00\x08\x00'\
b'\x00\x36\x36\x6c\x00\x00\x00\x00\x09\x00\x00\x00\x36\x00\x7f\x00'\
b'\x36\x00\x36\x00\x7f\x00\x36\x00\x00\x00\x09\x00\x00\x00\x3e\x00'\
b'\x6b\x00\x68\x00\x3e\x00\x0b\x00\x6b\x00\x3e\x00\x0a\x00\x23\x00'\
b'\x56\x00\x76\x00\x2c\x00\x0d\x00\x1a\x80\x1b\x80\x31\x00\x09\x00'\
b'\x38\x00\x6c\x00\x6c\x00\x38\x00\x6f\x00\x66\x00\x3f\x00\x00\x00'\
b'\x05\x00\x00\x30\x30\x60\x00\x00\x00\x00\x05\x00\x30\x60\x60\x60'\
b'\x60\x60\x30\x00\x05\x00\x60\x30\x30\x30\x30\x30\x60\x00\x08\x00'\
b'\x00\x18\x7e\x3c\x7e\x18\x00\x00\x08\x00\x00\x18\x18\x7e\x18\x18'\
b'\x00\x00\x05\x00\x00\x00\x00\x00\x00\x30\x30\x60\x07\x00\x00\x00'\
b'\x00\x00\x7c\x00\x00\x00\x04\x00\x00\x00\x00\x00\x00\x60\x60\x00'\
b'\x07\x00\x0c\x0c\x18\x18\x30\x30\x60\x60\x08\x00\x3c\x66\x76\x7e'\
b'\x6e\x66\x3c\x00\x08\x00\x18\x78\x18\x18\x18\x18\x7e\x00\x08\x00'\
b'\x3c\x66\x06\x0c\x18\x30\x7e\x00\x08\x00\x7c\x06\x06\x3c\x06\x06'\
b'\x7c\x00\x08\x00\x0c\x1c\x3c\x6c\x7e\x0c\x0c\x00\x08\x00\x7e\x60'\
b'\x7c\x06\x06\x66\x3c\x00\x08\x00\x3c\x66\x60\x7c\x66\x66\x3c\x00'\
b'\x08\x00\x7e\x66\x06\x0c\x18\x30\x30\x00\x08\x00\x3c\x66\x66\x3c'\
b'\x66\x66\x3c\x00\x08\x00\x3c\x66\x66\x3e\x06\x0c\x38\x00\x04\x00'\
b'\x00\x00\x60\x00\x00\x60\x00\x00\x05\x00\x00\x00\x00\x30\x00\x30'\
b'\x30\x60\x06\x00\x00\x18\x30\x60\x30\x18\x00\x00\x09\x00\x00\x00'\
b'\x00\x00\x7f\x00\x00\x00\x7f\x00\x00\x00\x00\x00\x00\x00\x06\x00'\
b'\x00\x60\x30\x18\x30\x60\x00\x00\x08\x00\x3c\x66\x06\x1c\x30\x00'\
b'\x18\x00\x0a\x00\x3e\x00\x63\x00\x7f\x00\x6f\x00\x7e\x00\x61\x80'\
b'\x3f\x00\x00\x00\x08\x00\x3c\x66\x66\x66\x7e\x66\x66\x00\x08\x00'\
b'\x7c\x66\x66\x7c\x66\x66\x7c\x00\x08\x00\x3c\x66\x60\x60\x60\x66'\
b'\x3c\x00\x09\x00\x7e\x00\x33\x00\x33\x00\x33\x00\x33\x00\x33\x00'\
b'\x3e\x00\x00\x00\x08\x00\x7e\x60\x60\x7c\x60\x60\x7e\x00\x08\x00'\
b'\x7e\x60\x60\x7c\x60\x60\x60\x00\x08\x00\x3e\x60\x60\x6e\x66\x66'\
b'\x3e\x00\x08\x00\x66\x66\x66\x7e\x66\x66\x66\x00\x06\x00\x78\x30'\
b'\x30\x30\x30\x30\x78\x00\x07\x00\x7c\x18\x18\x18\x18\x18\x70\x00'\
b'\x08\x00\x66\x6c\x78\x78\x6c\x6c\x66\x00\x07\x00\x60\x60\x60\x60'\
b'\x60\x60\x7c\x00\x09\x00\x63\x00\x63\x00\x77\x00\x7f\x00\x63\x00'\
b'\x63\x00\x63\x00\x00\x00\x08\x00\x66\x76\x7e\x6e\x66\x66\x66\x00'\
b'\x08\x00\x3c\x66\x66\x66\x66\x66\x3c\x00\x08\x00\x7c\x66\x66\x7c'\
b'\x60\x60\x60\x00\x08\x00\x3c\x66\x66\x66\x76\x6c\x3e\x00\x08\x00'\
b'\x7c\x66\x66\x7c\x6c\x66\x66\x00\x08\x00\x3c\x66\x60\x3c\x06\x66'\
b'\x3c\x00\x08\x00\x7e\x18\x18\x18\x18\x18\x18\x00\x08\x00\x66\x66'\
b'\x66\x66\x66\x66\x3c\x00\x08\x00\x66\x66\x66\x66\x3c\x3c\x18\x00'\
b'\x0a\x00\x61\x80\x61\x80\x6d\x80\x6d\x80\x6d\x80\x7f\x80\x33\x00'\
b'\x00\x00\x08\x00\x66\x66\x3c\x18\x3c\x66\x66\x00\x08\x00\x66\x66'\
b'\x66\x3c\x18\x18\x18\x00\x08\x00\x7e\x06\x0c\x18\x30\x60\x7e\x00'\
b'\x05\x00\x70\x60\x60\x60\x60\x60\x70\x00\x07\x00\x60\x60\x30\x30'\
b'\x18\x18\x0c\x0c\x05\x00\x70\x30\x30\x30\x30\x30\x70\x00\x07\x00'\
b'\x00\x38\x6c\x00\x00\x00\x00\x00\x09\x00\x00\x00\x00\x00\x00\x00'\
b'\x00\x00\x00\x00\x00\x00\x00\x00\x7f\x00\x05\x00\x00\x60\x30\x30'\
b'\x00\x00\x00\x00\x08\x00\x00\x3c\x06\x3e\x66\x66\x3e\x00\x08\x00'\
b'\x60\x60\x7c\x66\x66\x66\x7c\x00\x08\x00\x00\x3c\x66\x60\x60\x60'\
b'\x3e\x00\x08\x00\x06\x06\x3e\x66\x66\x66\x3e\x00\x08\x00\x00\x3c'\
b'\x66\x7e\x60\x60\x3e\x00\x08\x00\x1e\x30\x30\x7c\x30\x30\x30\x00'\
b'\x08\x00\x00\x3e\x66\x66\x66\x3e\x06\x3c\x08\x00\x60\x60\x7c\x76'\
b'\x66\x66\x66\x00\x06\x00\x30\x00\x70\x30\x30\x30\x78\x00\x06\x00'\
b'\x00\x78\x30\x30\x30\x30\x30\x60\x08\x00\x00\x66\x6c\x78\x78\x6c'\
b'\x66\x00\x06\x00\x00\x60\x60\x60\x60\x60\x38\x00\x0a\x00\x00\x00'\
b'\x3f\x00\x6d\x80\x6d\x80\x6d\x80\x6d\x80\x6d\x80\x00\x00\x08\x00'\
b'\x00\x1c\x76\x66\x66\x66\x66\x00\x08\x00\x00\x3c\x66\x66\x66\x66'\
b'\x3c\x00\x08\x00\x00\x7c\x66\x66\x66\x7c\x60\x60\x08\x00\x00\x3e'\
b'\x66\x66\x66\x3e\x06\x06\x07\x00\x00\x3c\x70\x60\x60\x60\x60\x00'\
b'\x08\x00\x00\x3c\x60\x3c\x06\x66\x3c\x00\x07\x00\x60\x60\x7c\x60'\
b'\x60\x60\x3c\x00\x08\x00\x00\x66\x66\x66\x66\x6e\x3e\x00\x08\x00'\
b'\x00\x66\x66\x66\x3c\x3c\x18\x00\x0a\x00\x00\x00\x01\x80\x6d\x80'\
b'\x6d\x80\x6d\x80\x6d\x80\x3f\x00\x00\x00\x08\x00\x00\x66\x3c\x18'\
b'\x18\x3c\x66\x00\x07\x00\x00\x6c\x6c\x6c\x38\x18\x70\x00\x07\x00'\
b'\x00\x7c\x0c\x18\x30\x60\x7c\x00\x07\x00\x18\x34\x20\x60\x60\x20'\
b'\x34\x18\x04\x00\x60\x60\x60\x60\x60\x60\x60\x60\x07\x00\x30\x58'\
b'\x08\x0c\x0c\x08\x58\x30\x09\x00\x00\x00\x00\x00\x00\x00\x3b\x00'\
b'\x6e\x00\x00\x00\x00\x00\x00\x00'

_index =\
b'\x00\x00\x0a\x00\x14\x00\x1e\x00\x28\x00\x3a\x00\x4c\x00\x5e\x00'\
b'\x70\x00\x7a\x00\x84\x00\x8e\x00\x98\x00\xa2\x00\xac\x00\xb6\x00'\
b'\xc0\x00\xca\x00\xd4\x00\xde\x00\xe8\x00\xf2\x00\xfc\x00\x06\x01'\
b'\x10\x01\x1a\x01\x24\x01\x2e\x01\x38\x01\x42\x01\x4c\x01\x5e\x01'\
b'\x68\x01\x72\x01\x84\x01\x8e\x01\x98\x01\xa2\x01\xb4\x01\xbe\x01'\
b'\xc8\x01\xd2\x01\xdc\x01\xe6\x01\xf0\x01\xfa\x01\x04\x02\x16\x02'\
b'\x20\x02\x2a\x02\x34\x02\x3e\x02\x48\x02\x52\x02\x5c\x02\x66\x02'\
b'\x70\x02\x82\x02\x8c\x02\x96\x02\xa0\x02\xaa\x02\xb4\x02\xbe\x02'\
b'\xc8\x02\xda\x02\xe4\x02\xee\x02\xf8\x02\x02\x03\x0c\x03\x16\x03'\
b'\x20\x03\x2a\x03\x34\x03\x3e\x03\x48\x03\x52\x03\x5c\x03\x6e\x03'\
b'\x78\x03\x82\x03\x8c\x03\x96\x03\xa0\x03\xaa\x03\xb4\x03\xbe\x03'\
b'\xc8\x03\xda\x03\xe4\x03\xee\x03\xf8\x03\x02\x04\x0c\x04\x16\x04'\
b'\x28\x04'

_mvfont = memoryview(_font)
_mvi = memoryview(_index)
ifb = lambda l : l[0] | (l[1] << 8)

def get_ch(ch):
    oc = ord(ch)
    ioff = 2 * (oc - 32 + 1) if oc >= 32 and oc <= 126 else 0
    doff = ifb(_mvi[ioff : ])
    width = ifb(_mvfont[doff : ])

    next_offs = doff + 2 + ((width - 1)//8 + 1) * 8
    return _mvfont[doff + 2:next_offs], 8, width
 
