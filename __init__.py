import display
import sys
import time
import buttons as b
import framebuf
import png
import gc
from color import Color

sys.path.append("/apps/pixelfont_gallery/")
import writer_card10 as w

fontid = 3
lastfontid = fontid
fontcount = 7

modeid = 1
lastmodeid = modeid
modecount = 3

buffer = framebuf.FrameBuffer(bytearray(160 * 24 * 2), 160, 24, framebuf.RGB565)
multitextbuffer = w.MultilineTextBuffer(scroll=False)
textbuffer = None
fontname = None
sysfont = None
currentfont = None
boldfont = None
buttons = 0
lastbuttons = 0

def update_font():
    global sysfont, fontname, currentfont, boldfont
    if fontid == 0:
        import Cozette13
        currentfont = Cozette13
        boldfont = Cozette13
        fontname = "Cozette 13"
        sysfont = display.FONT12
    elif fontid == 1:
        import Dogica8
        import DogicaBold8
        currentfont = Dogica8
        boldfont = DogicaBold8
        fontname = "Dogica 8"
        sysfont = display.FONT8
    elif fontid == 2:
        import Gohufont11
        import GohufontBold11
        currentfont = Gohufont11
        boldfont = GohufontBold11
        fontname = "Gohufont 11"
        sysfont = display.FONT12
    elif fontid == 3:
        import Gohufont14
        import GohufontBold14
        currentfont = Gohufont14
        boldfont = GohufontBold14
        fontname = "Gohufont 14"
        sysfont = display.FONT12
    elif fontid == 4:
        import Peaberry16
        currentfont = Peaberry16
        boldfont = Peaberry16
        fontname = "Peaberry 16"
        sysfont = display.FONT16
    elif fontid == 5:
        import RaccoonSerifBase16
        import RaccoonSerifBold16
        #import RaccoonSerifMedium16
        currentfont = RaccoonSerifBase16
        boldfont = RaccoonSerifBold16
        fontname = "Raccoon Serif 16"
        sysfont = display.FONT16
    elif fontid == 6:
        import RaccoonSerifMini12
        currentfont = RaccoonSerifMini12
        boldfont = RaccoonSerifMini12
        fontname = "Raccoon Serif Mini 12"
        sysfont = display.FONT12
    update_mode()

def update_mode():
    global buffer, textbuffer
    if modeid == 0:
        textbuffer = w.TextBuffer(currentfont,160,24,buffer)
    elif modeid == 1:
        multitextbuffer.clear()
        multitextbuffer.writer(currentfont).printstring(f"{fontname}. Left and right buttons switch fonts. Top right button changes mode.")
    elif modeid == 2:
        multitextbuffer.clear()
        writer = multitextbuffer.writer(currentfont)
        boldwriter = multitextbuffer.writer(boldfont)
        boldwriter.printstring(f"{fontname}\n")
        writer.printstring("Some fonts have a ")
        boldwriter.printstring("bold variant ")
        writer.printstring("that can be used.")


update_font()


# disable garbage collector for more accurate time measurement
gc.disable()

with display.open() as d:
    loopstart = 0
    loopend = 0
    lastloop = 0

    while True:
        loopstart = time.ticks_ms()

        d.clear()

        if modeid != 0:
            multitextbuffer.render(d)
        else:
            system_renderer_time = -time.ticks_ms()
            d.print("System Font", posx=0, posy=0, font=sysfont)
            system_renderer_time += time.ticks_ms()

            custom_renderer_time = -time.ticks_ms()
            textbuffer.settext(fontname)
            textbuffer.dirty = True # render again for timing
            textbuffer.render(d, 0, 30)
            custom_renderer_time += time.ticks_ms()

            d.print(f"^{system_renderer_time : >3}ms   v{custom_renderer_time :>3}ms", posx=0, posy=60)

        d.update()

        loopend = time.ticks_ms()
        lastloop = loopend - loopstart

        lastbuttons = buttons
        lastfontid = fontid
        lastmodeid = modeid

        buttons = b.read(b.BOTTOM_LEFT | b.BOTTOM_RIGHT | b.TOP_RIGHT)

        if not (lastbuttons & b.BOTTOM_LEFT) and (buttons & b.BOTTOM_LEFT):
            fontid -= 1

        if not (lastbuttons & b.BOTTOM_RIGHT) and (buttons & b.BOTTOM_RIGHT):
            fontid += 1

        if not (lastbuttons & b.TOP_RIGHT) and (buttons & b.TOP_RIGHT):
            modeid += 1
            modeid = modeid % modecount

        if lastfontid != fontid:
            fontid = fontid % fontcount
            update_font()
        elif lastmodeid != modeid:
            update_mode()

        gc.collect()
        time.sleep_ms(100)

