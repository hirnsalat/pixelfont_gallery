# Code generated by font_to_py.py.
# Font: RaccoonSerifMedium16.ttf
# Cmd: ../font_to_py.py -x in/RaccoonSerifMedium16.ttf 16 out/RaccoonSerifMedium16.py
version = '0.33'

def height():
    return 16

def baseline():
    return 12

def max_width():
    return 16

def hmap():
    return True

def reverse():
    return False

def monospaced():
    return False

def min_ch():
    return 32

def max_ch():
    return 126

_font =\
b'\x09\x00\x1c\x00\x22\x00\x41\x00\x61\x00\x01\x00\x02\x00\x0c\x00'\
b'\x18\x00\x18\x00\x00\x00\x18\x00\x18\x00\x00\x00\x00\x00\x00\x00'\
b'\x00\x00\x05\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00'\
b'\x00\x00\x00\x00\x04\x00\x00\x60\x60\x60\x60\x60\x60\x60\x00\x00'\
b'\x60\x60\x00\x00\x00\x00\x08\x00\x00\xee\xee\xee\x44\x44\x00\x00'\
b'\x00\x00\x00\x00\x00\x00\x00\x00\x0d\x00\x00\x00\x06\x60\x06\x60'\
b'\x0c\xc0\x7f\xf0\x19\xc0\x19\x80\x33\x00\xff\xe0\x33\x00\x66\x00'\
b'\x66\x00\x00\x00\x00\x00\x00\x00\x00\x00\x0b\x00\x0c\x00\x3f\x00'\
b'\x6d\x80\xcc\xc0\xcc\xc0\x6c\x00\x3f\x00\x0d\x80\x0c\xc0\xcc\xc0'\
b'\xcc\xc0\x6d\x80\x3f\x00\x0c\x00\x00\x00\x00\x00\x0f\x00\x00\x30'\
b'\x3c\x60\x66\x60\x66\xc0\x66\xc0\x3d\x80\x03\x00\x03\x78\x06\xcc'\
b'\x0c\xcc\x0c\xcc\x18\x78\x00\x00\x00\x00\x00\x00\x00\x00\x0c\x00'\
b'\x00\x00\x1f\x00\x31\x80\x31\x80\x1b\x00\x1e\x00\x36\x40\x63\x80'\
b'\x61\x80\x61\xc0\x33\x60\x1e\x20\x00\x00\x00\x00\x00\x00\x00\x00'\
b'\x04\x00\xe0\xe0\xe0\x40\x40\x00\x00\x00\x00\x00\x00\x00\x00\x00'\
b'\x00\x00\x06\x00\x18\x30\x60\x60\x60\x60\x60\x60\x60\x60\x60\x60'\
b'\x30\x18\x00\x00\x05\x00\xc0\x60\x30\x30\x30\x30\x30\x30\x30\x30'\
b'\x30\x30\x60\xc0\x00\x00\x08\x00\x10\x54\x38\xfe\x38\x54\x10\x00'\
b'\x00\x00\x00\x00\x00\x00\x00\x00\x09\x00\x00\x00\x00\x00\x00\x00'\
b'\x08\x00\x08\x00\x08\x00\x7f\x00\x08\x00\x08\x00\x08\x00\x00\x00'\
b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x04\x00\x00\x00\x00\x00'\
b'\x00\x00\x00\x00\x00\x00\x60\x60\x20\x40\x00\x00\x06\x00\x00\x00'\
b'\x00\x00\x00\x00\x00\xf8\x00\x00\x00\x00\x00\x00\x00\x00\x04\x00'\
b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x60\x60\x00\x00\x00\x00'\
b'\x07\x00\x00\x04\x04\x08\x08\x10\x10\x20\x20\x40\x40\x80\x00\x00'\
b'\x00\x00\x0a\x00\x00\x00\x3e\x00\x63\x00\xc5\x80\xc5\x80\xc9\x80'\
b'\xc9\x80\xd1\x80\xd1\x80\xe1\x80\x63\x00\x3e\x00\x00\x00\x00\x00'\
b'\x00\x00\x00\x00\x0b\x00\x00\x00\x0e\x00\x16\x00\x26\x00\x06\x00'\
b'\x06\x00\x06\x00\x06\x00\x06\x00\x06\x00\x06\x00\x3f\xc0\x00\x00'\
b'\x00\x00\x00\x00\x00\x00\x0a\x00\x00\x00\x3f\x00\x61\x80\x61\x80'\
b'\x01\x80\x03\x00\x06\x00\x1c\x00\x38\x00\x60\x00\x60\x80\x7f\x80'\
b'\x00\x00\x00\x00\x00\x00\x00\x00\x0a\x00\x00\x00\x3f\x00\x61\x80'\
b'\x61\x80\x01\x80\x03\x00\x1f\x00\x01\x80\x01\x80\x61\x80\x61\x80'\
b'\x3f\x00\x00\x00\x00\x00\x00\x00\x00\x00\x0a\x00\x00\x00\x03\x00'\
b'\x07\x00\x0f\x00\x0f\x00\x1b\x00\x1b\x00\x33\x00\x7f\x80\x03\x00'\
b'\x03\x00\x07\x80\x00\x00\x00\x00\x00\x00\x00\x00\x0b\x00\x00\x00'\
b'\x7f\x80\x60\x00\x60\x00\x7f\x00\x61\x80\x40\xc0\x00\xc0\x00\xc0'\
b'\x40\xc0\x61\x80\x3f\x00\x00\x00\x00\x00\x00\x00\x00\x00\x0a\x00'\
b'\x00\x00\x3e\x00\x63\x00\x61\x80\xc0\x00\xfe\x00\xc3\x00\xc1\x80'\
b'\xc1\x80\xc1\x80\x63\x00\x3e\x00\x00\x00\x00\x00\x00\x00\x00\x00'\
b'\x0a\x00\x00\x00\x7f\x80\x01\x80\x03\x00\x06\x00\x06\x00\x0c\x00'\
b'\x18\x00\x18\x00\x30\x00\x30\x00\x30\x00\x00\x00\x00\x00\x00\x00'\
b'\x00\x00\x0a\x00\x00\x00\x3e\x00\x63\x00\x63\x00\x63\x00\x3e\x00'\
b'\x63\x00\xc1\x80\xc1\x80\xc1\x80\x63\x00\x3e\x00\x00\x00\x00\x00'\
b'\x00\x00\x00\x00\x0a\x00\x00\x00\x3e\x00\x63\x00\xc1\x80\xc1\x80'\
b'\xc1\x80\x63\x80\x3d\x80\x01\x80\x01\x80\x63\x00\x3e\x00\x00\x00'\
b'\x00\x00\x00\x00\x00\x00\x04\x00\x00\x00\x00\x00\x60\x60\x00\x00'\
b'\x00\x00\x60\x60\x00\x00\x00\x00\x04\x00\x00\x00\x00\x00\x60\x60'\
b'\x00\x00\x00\x00\x60\x60\x20\x40\x00\x00\x0b\x00\x00\x00\x00\x00'\
b'\x00\xc0\x03\x80\x0e\x00\x38\x00\x60\x00\x38\x00\x0e\x00\x03\x80'\
b'\x00\xc0\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x09\x00\x00\x00'\
b'\x00\x00\x00\x00\x00\x00\x7f\x00\x00\x00\x00\x00\x00\x00\x7f\x00'\
b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x0a\x00'\
b'\x00\x00\x00\x00\x00\x00\x70\x00\x1c\x00\x07\x00\x01\x80\x07\x00'\
b'\x1c\x00\x70\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00'\
b'\x09\x00\x1c\x00\x22\x00\x41\x00\x61\x00\x01\x00\x02\x00\x0c\x00'\
b'\x18\x00\x18\x00\x00\x00\x18\x00\x18\x00\x00\x00\x00\x00\x00\x00'\
b'\x00\x00\x0f\x00\x00\x00\x0f\xe0\x10\x10\x20\x08\x47\xa4\x48\x44'\
b'\x50\x44\x50\x44\x50\x44\x50\x44\x48\x64\x47\x98\x20\x00\x10\x08'\
b'\x0f\xf0\x00\x00\x0d\x00\x06\x00\x0f\x00\x0f\x00\x19\x80\x19\x80'\
b'\x30\xc0\x3f\xc0\x60\x60\x60\x60\x60\x60\x60\x60\xe0\x70\x00\x00'\
b'\x00\x00\x00\x00\x00\x00\x0c\x00\x7f\x00\x61\x80\x60\xc0\x60\xc0'\
b'\x60\xc0\x7f\x80\x60\xc0\x60\x60\x60\x60\x60\x60\x60\xc0\x7f\x80'\
b'\x00\x00\x00\x00\x00\x00\x00\x00\x0d\x00\x1f\x80\x30\xc0\x60\x60'\
b'\xc0\x70\xc0\x00\xc0\x00\xc0\x00\xc0\x00\xc0\x70\x60\x60\x30\xc0'\
b'\x1f\x80\x00\x00\x00\x00\x00\x00\x00\x00\x0d\x00\x7f\x80\x60\xc0'\
b'\x60\x60\x60\x30\x60\x30\x60\x30\x60\x30\x60\x30\x60\x30\x60\x60'\
b'\x60\xc0\x7f\x80\x00\x00\x00\x00\x00\x00\x00\x00\x0c\x00\xff\xe0'\
b'\x60\x60\x60\x00\x60\x00\x60\x00\x7f\x00\x60\x00\x60\x00\x60\x00'\
b'\x60\x00\x60\x60\xff\xe0\x00\x00\x00\x00\x00\x00\x00\x00\x0a\x00'\
b'\xff\x80\x60\x80\x60\x00\x60\x00\x60\x00\x7e\x00\x60\x00\x60\x00'\
b'\x60\x00\x60\x00\x60\x00\xf0\x00\x00\x00\x00\x00\x00\x00\x00\x00'\
b'\x0e\x00\x1f\xe0\x30\x30\x60\x30\xc0\x00\xc0\x00\xc0\x00\xc3\xf8'\
b'\xc0\x30\xc0\x30\x60\x30\x30\x60\x1f\xc0\x00\x00\x00\x00\x00\x00'\
b'\x00\x00\x0d\x00\xf0\xf0\x60\x60\x60\x60\x60\x60\x60\x60\x7f\xe0'\
b'\x60\x60\x60\x60\x60\x60\x60\x60\x60\x60\xf0\xf0\x00\x00\x00\x00'\
b'\x00\x00\x00\x00\x09\x00\xff\x00\x18\x00\x18\x00\x18\x00\x18\x00'\
b'\x18\x00\x18\x00\x18\x00\x18\x00\x18\x00\x18\x00\xff\x00\x00\x00'\
b'\x00\x00\x00\x00\x00\x00\x0d\x00\x0f\xf0\x01\x80\x01\x80\x01\x80'\
b'\x01\x80\x01\x80\x01\x80\x01\x80\xc1\x80\xc1\x80\x63\x00\x3e\x00'\
b'\x00\x00\x00\x00\x00\x00\x00\x00\x0d\x00\xe0\xe0\x61\x80\x63\x00'\
b'\x66\x00\x6c\x00\x7c\x00\x66\x00\x63\x00\x61\x80\x60\xc0\x60\x60'\
b'\xe0\x70\x00\x00\x00\x00\x00\x00\x00\x00\x0a\x00\xf0\x00\x60\x00'\
b'\x60\x00\x60\x00\x60\x00\x60\x00\x60\x00\x60\x00\x60\x00\x60\x00'\
b'\x60\x80\x7f\x80\x00\x00\x00\x00\x00\x00\x00\x00\x0f\x00\x60\x18'\
b'\x70\x38\x78\x78\x6c\xd8\x6c\xd8\x67\x98\x67\x98\x62\x18\x62\x18'\
b'\x60\x18\x60\x18\xe0\x3c\x00\x00\x00\x00\x00\x00\x00\x00\x0d\x00'\
b'\x60\xf0\x70\x60\x78\x60\x6c\x60\x6c\x60\x66\x60\x63\x60\x63\x60'\
b'\x61\xe0\x60\xe0\x60\xe0\xf0\x60\x00\x00\x00\x00\x00\x00\x00\x00'\
b'\x0e\x00\x0f\x80\x38\xe0\x60\x30\x60\x30\xc0\x18\xc0\x18\xc0\x18'\
b'\xc0\x18\x60\x30\x60\x30\x38\xe0\x0f\x80\x00\x00\x00\x00\x00\x00'\
b'\x00\x00\x0c\x00\x7f\x80\x60\xc0\x60\x60\x60\x60\x60\x60\x60\xc0'\
b'\x7f\x80\x60\x00\x60\x00\x60\x00\x60\x00\xf0\x00\x00\x00\x00\x00'\
b'\x00\x00\x00\x00\x0d\x00\x1f\x80\x30\xc0\x60\x60\x60\x30\xc0\x30'\
b'\xc0\x30\xc0\x30\xc2\x30\x63\x30\x61\xe0\x30\xc0\x1f\xe0\x00\x30'\
b'\x00\x00\x00\x00\x00\x00\x0d\x00\x7f\x80\x60\xc0\x60\x60\x60\x60'\
b'\x60\x60\x60\xc0\x7f\x80\x61\x80\x60\xc0\x60\xc0\x60\x60\xf0\xf0'\
b'\x00\x00\x00\x00\x00\x00\x00\x00\x0c\x00\x1f\x00\x31\x80\x60\xc0'\
b'\x60\xe0\x30\x00\x1c\x00\x07\x00\x01\x80\xe0\xc0\x60\xc0\x31\x80'\
b'\x1f\x00\x00\x00\x00\x00\x00\x00\x00\x00\x0b\x00\xff\xc0\x0c\x00'\
b'\x0c\x00\x0c\x00\x0c\x00\x0c\x00\x0c\x00\x0c\x00\x0c\x00\x0c\x00'\
b'\x0c\x00\x0c\x00\x00\x00\x00\x00\x00\x00\x00\x00\x0e\x00\xf0\x78'\
b'\x60\x30\x60\x30\x60\x30\x60\x30\x60\x30\x60\x30\x60\x30\x60\x30'\
b'\x60\x30\x30\xc0\x1f\x80\x00\x00\x00\x00\x00\x00\x00\x00\x0f\x00'\
b'\xf0\x3c\x60\x18\x30\x30\x30\x30\x18\x60\x18\x60\x18\x60\x0c\xc0'\
b'\x0c\xc0\x07\x80\x07\x80\x02\x00\x00\x00\x00\x00\x00\x00\x00\x00'\
b'\x11\x00\xe0\x07\x00\x60\x06\x00\x60\x06\x00\x30\x0c\x00\x31\x8c'\
b'\x00\x31\x8c\x00\x19\x98\x00\x1b\xd8\x00\x1b\xd8\x00\x1e\x78\x00'\
b'\x0c\x30\x00\x0c\x30\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00'\
b'\x00\x00\x0d\x00\xe0\x70\x60\x60\x30\xc0\x30\xc0\x19\x80\x0f\x00'\
b'\x06\x00\x0f\x00\x19\x80\x30\xc0\x60\x60\xe0\x70\x00\x00\x00\x00'\
b'\x00\x00\x00\x00\x0d\x00\xe0\x70\x60\x60\x30\xc0\x30\xc0\x19\x80'\
b'\x0f\x00\x06\x00\x06\x00\x06\x00\x06\x00\x06\x00\x0f\x00\x00\x00'\
b'\x00\x00\x00\x00\x00\x00\x0c\x00\xff\xe0\xc0\x60\x00\xc0\x01\x80'\
b'\x03\x00\x06\x00\x0c\x00\x18\x00\x30\x00\x60\x00\xc0\x60\xff\xe0'\
b'\x00\x00\x00\x00\x00\x00\x00\x00\x06\x00\x78\x60\x60\x60\x60\x60'\
b'\x60\x60\x60\x60\x60\x60\x60\x60\x78\x00\x07\x00\x00\x80\x80\x40'\
b'\x40\x20\x20\x10\x10\x08\x08\x04\x00\x00\x00\x00\x05\x00\xf0\x30'\
b'\x30\x30\x30\x30\x30\x30\x30\x30\x30\x30\x30\x30\xf0\x00\x08\x00'\
b'\x10\x10\x28\x44\x44\x82\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00'\
b'\x0a\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00'\
b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\xff\x80\x00\x00\x00\x00'\
b'\x00\x00\x07\x00\x00\x70\x38\x18\x0c\x04\x00\x00\x00\x00\x00\x00'\
b'\x00\x00\x00\x00\x0a\x00\x00\x00\x00\x00\x00\x00\x00\x00\x3e\x00'\
b'\x63\x00\x03\x00\x3f\x00\x63\x00\x63\x00\x63\x00\x3f\x80\x00\x00'\
b'\x00\x00\x00\x00\x00\x00\x0a\x00\xe0\x00\x60\x00\x60\x00\x60\x00'\
b'\x7e\x00\x63\x00\x61\x80\x61\x80\x61\x80\x61\x80\x63\x00\xde\x00'\
b'\x00\x00\x00\x00\x00\x00\x00\x00\x09\x00\x00\x00\x00\x00\x00\x00'\
b'\x00\x00\x3e\x00\x63\x00\xc0\x00\xc0\x00\xc0\x00\xc3\x00\x62\x00'\
b'\x3c\x00\x00\x00\x00\x00\x00\x00\x00\x00\x0b\x00\x03\x80\x01\x80'\
b'\x01\x80\x01\x80\x3d\x80\x63\x80\xc1\x80\xc1\x80\xc1\x80\xc3\x80'\
b'\x63\x80\x3d\xc0\x00\x00\x00\x00\x00\x00\x00\x00\x09\x00\x00\x00'\
b'\x00\x00\x00\x00\x00\x00\x3e\x00\x63\x00\xc3\x00\xff\x00\xc0\x00'\
b'\xc0\x00\x63\x00\x3e\x00\x00\x00\x00\x00\x00\x00\x00\x00\x09\x00'\
b'\x1e\x00\x33\x00\x33\x00\x30\x00\x30\x00\xfe\x00\x30\x00\x30\x00'\
b'\x30\x00\x30\x00\x30\x00\x78\x00\x00\x00\x00\x00\x00\x00\x00\x00'\
b'\x0b\x00\x00\x00\x00\x00\x00\x00\x00\x00\x3f\xc0\x63\x80\xc1\x80'\
b'\xc1\x80\xc1\x80\xc1\x80\x63\x80\x3d\x80\x01\x80\x01\x80\x63\x00'\
b'\x3e\x00\x0b\x00\xe0\x00\x60\x00\x60\x00\x60\x00\x7e\x00\x63\x00'\
b'\x61\x80\x61\x80\x61\x80\x61\x80\x61\x80\xe1\xc0\x00\x00\x00\x00'\
b'\x00\x00\x00\x00\x05\x00\x60\x60\x00\x00\xe0\x60\x60\x60\x60\x60'\
b'\x60\xf0\x00\x00\x00\x00\x07\x00\x0c\x0c\x00\x00\x1c\x0c\x0c\x0c'\
b'\x0c\x0c\x0c\x0c\x0c\x0c\xcc\x78\x0b\x00\x70\x00\x30\x00\x31\x80'\
b'\x33\x00\x36\x00\x3c\x00\x38\x00\x38\x00\x3c\x00\x36\x00\x33\x00'\
b'\x71\xc0\x00\x00\x00\x00\x00\x00\x00\x00\x05\x00\xe0\x60\x60\x60'\
b'\x60\x60\x60\x60\x60\x60\x60\xf0\x00\x00\x00\x00\x10\x00\x00\x00'\
b'\x00\x00\x00\x00\x00\x00\xfe\xf0\x63\x98\x63\x0c\x63\x0c\x63\x0c'\
b'\x63\x0c\x63\x0c\xf3\x8e\x00\x00\x00\x00\x00\x00\x00\x00\x0b\x00'\
b'\x00\x00\x00\x00\x00\x00\x00\x00\xff\x00\x71\x80\x61\x80\x61\x80'\
b'\x61\x80\x61\x80\x61\x80\xf1\xc0\x00\x00\x00\x00\x00\x00\x00\x00'\
b'\x0b\x00\x00\x00\x00\x00\x00\x00\x00\x00\x1f\x00\x31\x80\x60\xc0'\
b'\x60\xc0\x60\xc0\x60\xc0\x31\x80\x1f\x00\x00\x00\x00\x00\x00\x00'\
b'\x00\x00\x0a\x00\x00\x00\x00\x00\x00\x00\x00\x00\xfe\x00\x63\x00'\
b'\x61\x80\x61\x80\x61\x80\x61\x80\x63\x00\x7e\x00\x60\x00\x60\x00'\
b'\x60\x00\xf0\x00\x0a\x00\x00\x00\x00\x00\x00\x00\x00\x00\x3f\x80'\
b'\x63\x00\xc3\x00\xc3\x00\xc3\x00\xc3\x00\x63\x00\x3f\x00\x03\x00'\
b'\x03\x00\x03\x00\x07\x80\x09\x00\x00\x00\x00\x00\x00\x00\x00\x00'\
b'\xdc\x00\x62\x00\x63\x00\x60\x00\x60\x00\x60\x00\x60\x00\xf0\x00'\
b'\x00\x00\x00\x00\x00\x00\x00\x00\x09\x00\x00\x00\x00\x00\x00\x00'\
b'\x00\x00\x3c\x00\x63\x00\x60\x00\x38\x00\x0e\x00\x03\x00\x63\x00'\
b'\x3e\x00\x00\x00\x00\x00\x00\x00\x00\x00\x08\x00\x30\x30\x30\x30'\
b'\xfe\x30\x30\x30\x30\x30\x36\x1c\x00\x00\x00\x00\x0c\x00\x00\x00'\
b'\x00\x00\x00\x00\x00\x00\xe1\xc0\x60\xc0\x60\xc0\x60\xc0\x60\xc0'\
b'\x60\xc0\x31\xc0\x1e\xe0\x00\x00\x00\x00\x00\x00\x00\x00\x0b\x00'\
b'\x00\x00\x00\x00\x00\x00\x00\x00\xe1\xc0\x61\x80\x61\x80\x61\x80'\
b'\x33\x00\x33\x00\x1e\x00\x0c\x00\x00\x00\x00\x00\x00\x00\x00\x00'\
b'\x0d\x00\x00\x00\x00\x00\x00\x00\x00\x00\xe0\x70\x60\x60\x66\x60'\
b'\x66\x60\x3f\xc0\x3f\xc0\x19\x80\x19\x80\x00\x00\x00\x00\x00\x00'\
b'\x00\x00\x0a\x00\x00\x00\x00\x00\x00\x00\x00\x00\x73\x80\x33\x00'\
b'\x1e\x00\x0e\x00\x0e\x00\x1e\x00\x33\x00\x73\x80\x00\x00\x00\x00'\
b'\x00\x00\x00\x00\x0b\x00\x00\x00\x00\x00\x00\x00\x00\x00\xe1\xc0'\
b'\x61\x80\x61\x80\x33\x00\x33\x00\x1c\x00\x0c\x00\x0c\x00\x18\x00'\
b'\x30\x00\xe0\x00\x00\x00\x09\x00\x00\x00\x00\x00\x00\x00\x00\x00'\
b'\x7f\x00\x43\x00\x06\x00\x0c\x00\x18\x00\x30\x00\x61\x00\x7f\x00'\
b'\x00\x00\x00\x00\x00\x00\x00\x00\x07\x00\x1c\x30\x30\x30\x30\x30'\
b'\x60\xc0\x60\x30\x30\x30\x30\x30\x30\x1c\x04\x00\x20\x20\x20\x20'\
b'\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x00\x07\x00\xe0\x30'\
b'\x30\x30\x30\x30\x18\x0c\x18\x30\x30\x30\x30\x30\x30\xe0\x0e\x00'\
b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x3c\x00\x66\x18\xc3\x18'\
b'\xc3\x30\x01\xe0\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00'\

_index =\
b'\x00\x00\x22\x00\x34\x00\x46\x00\x58\x00\x7a\x00\x9c\x00\xbe\x00'\
b'\xe0\x00\xf2\x00\x04\x01\x16\x01\x28\x01\x4a\x01\x5c\x01\x6e\x01'\
b'\x80\x01\x92\x01\xb4\x01\xd6\x01\xf8\x01\x1a\x02\x3c\x02\x5e\x02'\
b'\x80\x02\xa2\x02\xc4\x02\xe6\x02\xf8\x02\x0a\x03\x2c\x03\x4e\x03'\
b'\x70\x03\x92\x03\xb4\x03\xd6\x03\xf8\x03\x1a\x04\x3c\x04\x5e\x04'\
b'\x80\x04\xa2\x04\xc4\x04\xe6\x04\x08\x05\x2a\x05\x4c\x05\x6e\x05'\
b'\x90\x05\xb2\x05\xd4\x05\xf6\x05\x18\x06\x3a\x06\x5c\x06\x7e\x06'\
b'\xa0\x06\xd2\x06\xf4\x06\x16\x07\x38\x07\x4a\x07\x5c\x07\x6e\x07'\
b'\x80\x07\xa2\x07\xb4\x07\xd6\x07\xf8\x07\x1a\x08\x3c\x08\x5e\x08'\
b'\x80\x08\xa2\x08\xc4\x08\xd6\x08\xe8\x08\x0a\x09\x1c\x09\x3e\x09'\
b'\x60\x09\x82\x09\xa4\x09\xc6\x09\xe8\x09\x0a\x0a\x1c\x0a\x3e\x0a'\
b'\x60\x0a\x82\x0a\xa4\x0a\xc6\x0a\xe8\x0a\xfa\x0a\x0c\x0b\x1e\x0b'\
b'\x40\x0b'

_mvfont = memoryview(_font)
_mvi = memoryview(_index)
ifb = lambda l : l[0] | (l[1] << 8)

def get_ch(ch):
    oc = ord(ch)
    ioff = 2 * (oc - 32 + 1) if oc >= 32 and oc <= 126 else 0
    doff = ifb(_mvi[ioff : ])
    width = ifb(_mvfont[doff : ])

    next_offs = doff + 2 + ((width - 1)//8 + 1) * 16
    return _mvfont[doff + 2:next_offs], 16, width
 
